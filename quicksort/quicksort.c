#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <time.h>

#define swap(m,n,type) {\
	    type _tmp;\
	    assert(sizeof(m) == sizeof(n));\
	    _tmp=m, m=n, n=_tmp;\
}

void quicksort(int *L,unsigned int size) {
	assert(L);

	if(size == 0) {
		return;
	} else {
		int mid = size - 1;
		unsigned int i = 0;
		while(i != mid) {
			while(i>mid && L[i] >= L[mid]) i--;
			while(i<mid && L[i] <= L[mid]) i++;
			swap(L[i], L[mid], int);
			swap(i, mid, int); 
			i += (i < mid)? 1 : ((i > mid) ? -1 : 0);
		}
		quicksort(L,mid);
		quicksort(L+mid+1,size-mid-1);
		return;
	}
	
}
int main() {
#define N 50
	int L[N],i;
	srand(time(NULL));
	for(i=0;i<N;i++) {
		L[i] = rand() % 100;
	}
	quicksort(L,N);
	for(i=0;i<N;i++) {
		printf("%02d ",L[i]);
	}
	putchar('\n');
}
